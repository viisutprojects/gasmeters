Startup
---
* Requirements for running the application:
    * Docker - https://docs.docker.com/engine/install/#server
    * Docker compose - https://docs.docker.com/compose/install/
* Installing
    * Clone the project `git clone git@bitbucket.org:viisutprojects/gasmeters.git`
    * Navigate into the project's folder `cd gasmeters` 
    * Execute `docker-compose up -d` to start the project
    * Execute `docker-compose restart php` after initial startup because of a problem with mysql not being accessible as quickly as needed.
* Components:
    * Frontend - http://localhost:32902/ (credentials are adminadmin:admin123)
    * ActiveMQ - http://localhost:8161/admin/topics.jsp (credentials are admin:admin)
    * PhpMyAdmin - http://localhost:32904/ (DB is lumen)
* Stack:
    * Webserver: HTTPd
    * Backend: PHP (Lumen framework)
    * Frontend: VueJs
    * Database: MySQL
    * Message broker: ActiveMQ

Notes
---
Initial startup might take some time since 3rd party libraries are downloading in the background. There might occur a problem with CORS validation after initial startup. If frontend's login form doesn't work excecute `docker-compose restart php
`. It's advised to use UNIX system (Mac, Linux etc.) for testing this project since line endings might be problematic on Windows OS. 

Initially 10 mocked clients and 10 mocked gas meters with some mock data are added. 
After logging in you can see the dashboard which contains 2 simple charts. Line chart which displays
gas consumption for last 60 minutes. This will keep updating on the fly every minute. Pie chart displays total
consumption for clients. Clients and gas meters can be managed in the settings area.

There's a cron job which publishes mock data to ActiveMQ every minute and subscriber which handles the data import.
Subscriber is currently a separate docker service. 

I commited vendor and node_modules to save some time when starting the project. Wouldn't recommend doing it for regular projects. 