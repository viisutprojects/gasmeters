<?php

use App\Models\GasMeter;
use App\Models\GasMeterRecord;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGasMeterRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gas_meter_records', function (Blueprint $table) {
            $table->id();
            $table->float('record');
            $table->float('increase');
            $table->foreignId('gas_meter_id')->constrained()->cascadeOnUpdate()->restrictOnDelete();
            $table->timestamp('timestamp');
            $table->timestamps();
            $table->softDeletes();
        });

        $this->createInitialData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gas_meter_records');
    }

    /**
     * Generate initial gas consumption records so we wouldn't have to wait for background task to generate it.
     *
     * @throws Exception
     */
    private function createInitialData(): void
    {
        $minutesToSimulate = 60;
        $currentTimeStamp = time();
        $insertData = [];
        /** @var GasMeter $meter */
        foreach (GasMeter::all() as $meter) {
            $gasConsumption = 0;
            for ($i = 0; $i < $minutesToSimulate; $i++) {
                // Generate new random gas consumption amount.
                $randomGasConsumption = random_int(5, 30);
                // Get time starting from earliest and moving closer to current time with each iteration.
                $timestamp = Carbon::createFromTimestamp($currentTimeStamp - ($minutesToSimulate * 60) + (60 * $i));
                $insertData[] = [
                    'gas_meter_id' => $meter->id,
                    'record' => $gasConsumption += $randomGasConsumption,
                    'increase' => $randomGasConsumption,
                    'timestamp' => $timestamp,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }
        }

        GasMeterRecord::insert($insertData);
    }
}
