<?php

use App\Models\Client;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('address');
            $table->timestamps();
            $table->softDeletes();
        });

        $this->createInitialData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }

    private function createInitialData(): void
    {
        $dummyClients = [
            'Albert Einstein' => '1300 Morris Park Ave, The Bronx, NY 10461, United States',
            'Blaise Pascal' => '27 Boulevard Blaise Pascal, 36000 Châteauroux, France',
            'Caroline Herschel' => '19 New King St, Bath BA1 2BL, United Kingdom',
            'Enrico Fermi' => '27 Poplar St, Yonkers, NY 10701, United States',
            'Erwin Schroedinger' => 'Boltzmanngasse 9A, 1090 Wien, Austria',
            'Geraldine Seydoux' => '1800 Orleans St, Baltimore, MD 21287, United States',
            'Johannes Kepler' => 'Zwillingestraße 21, 12057 Berlin, Germany',
            'Marie Curie' => 'Weimarische Str. 21, 10715 Berlin, Germany',
            'Michael Faraday' => 'Elephant and Castle, London SE1 6TG, United Kingdom',
            'Stephen Hawking' => '5 West Rd, Cambridge CB3 9DS, United Kingdom',
        ];

        foreach($dummyClients as $name => $addess) {
            $client = new Client();
            $client->full_name = $name;
            $client->address = $addess;
            $client->save();
        }
    }
}
