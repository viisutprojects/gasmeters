<?php

use App\Models\Client;
use App\Models\GasMeter;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGasMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gas_meters', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('client_id')->constrained()->cascadeOnUpdate()->restrictOnDelete();
            $table->softDeletes();
        });

        $this->createInitialData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gas_meters');
    }

    private function createInitialData(): void
    {
        /** @var Client $client */
        foreach(Client::all() as $client) {
            $gasMeter = new GasMeter();
            $gasMeter->client_id = $client->id;
            $gasMeter->save();
        }
    }
}
