<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


// Login urls
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
});

// Client CRUD routes
$router->group(['prefix' => 'api/client', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'ClientController@list');
    $router->get('/{id}', 'ClientController@get');
    $router->post('/', 'ClientController@create');
    $router->put('/{id}', 'ClientController@update');
    $router->delete('/{id}', 'ClientController@delete');
});

// Gas meter CRUD routes
$router->group(['prefix' => 'api/gas-meter', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'GasMeterController@list');
    $router->get('/{id}', 'GasMeterController@get');
    $router->post('/', 'GasMeterController@create');
    $router->put('/{id}', 'GasMeterController@update');
    $router->delete('/{id}', 'GasMeterController@delete');
});

// Gas meter records data routes
$router->group(['prefix' => 'api/data', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/hour', 'GasMeterRecordController@hour');
    $router->get('/client', 'GasMeterRecordController@totalConsumptionByClients');
});
