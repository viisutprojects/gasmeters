<?php

namespace App\Console\Commands;

use App\Http\Resources\MessageBroker;
use App\Models\GasMeter;
use App\Models\GasMeterRecord;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SubscribeToGasMeterUpdates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meter:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to message broker which releases updates gas meters\' readings';

    public function handle(MessageBroker $messageBroker): void
    {
        // Retry to connect 20 times. Wait 1 second between tries. Service might not be up yet.
        $connection = false;
        $counter = 0;
        while ($connection === false) {
            try {
                $connection = $messageBroker->connect();
                if (!$connection) {
                    if ($counter >= 20) {
                        exit(1);
                    }
                    sleep(1);
                }
            } catch (\Exception $ex) {
                /* TODO: add proper logging */
            }
        }

        $messageBroker->subscribe();
        $mqtt = $messageBroker->getMqtt();
        while ($data = $mqtt->proc()) {
            if (is_string($data)) {
                $this->createGasMeterReadingRecord($data);
            }
        }

        $mqtt->close();
    }

    private function createGasMeterReadingRecord(string $data): void
    {
        $data = json_decode($data);
        $gasMeter = GasMeter::find($data->id);
        if ($gasMeter && $data->id && $data->timestamp && $data->value) {
            $latestRecord = $gasMeter->records()->orderBy('timestamp', 'desc')->first();
            $reading = new GasMeterRecord();
            $reading->gas_meter_id = $data->id;
            $reading->timestamp = Carbon::createFromTimestamp($data->timestamp);
            $reading->increase = $data->value - ($latestRecord ? $latestRecord->record : 0);
            $reading->record = $data->value;
            $reading->save();
        }
    }
}

