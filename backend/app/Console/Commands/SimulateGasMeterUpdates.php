<?php

namespace App\Console\Commands;

use App\Http\Resources\MessageBroker;
use App\Models\GasMeter;
use App\Models\GasMeterRecord;
use Exception;
use Illuminate\Console\Command;

class SimulateGasMeterUpdates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meter:simulate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simulate gas meter updates and send mocked readings to message broker';

    /**
     * @param MessageBroker $messageBroker
     * @throws Exception
     */
    public function handle(MessageBroker $messageBroker): void
    {
        if ($messageBroker->connect()) {
            $data = $this->getGasMetersMockData();

            foreach ($data as $datum) {
                $messageBroker->publish($datum);
            }

            $messageBroker->disconnect();
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getGasMetersMockData(): array
    {
        $data = [];
        $gasMeters = GasMeter::all();

        foreach($gasMeters as $gasMeter) {
            /** @var GasMeterRecord $latestRecord */
            $latestRecord = $gasMeter->records()->orderBy('timestamp', 'desc')->first();
            $randomGasConsumption = random_int(5,30);
            $data[] = [
                'id' => $gasMeter->id,
                'timestamp' => time(),
                'value' => $latestRecord ? $latestRecord->record + $randomGasConsumption : $randomGasConsumption,
            ];
        }

        return $data;
    }
}
