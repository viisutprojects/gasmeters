<?php

namespace App\Console;

use App\Console\Commands\SimulateGasMeterUpdates;
use App\Console\Commands\SubscribeToGasMeterUpdates;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SimulateGasMeterUpdates::class,
        SubscribeToGasMeterUpdates::class,
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
