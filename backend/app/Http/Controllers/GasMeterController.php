<?php

namespace App\Http\Controllers;

use App\Models\GasMeter;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class GasMeterController extends Controller
{
    public function list(): JsonResponse
    {
        $gasMeters = GasMeter::all();

        foreach ($gasMeters as $gasMeter) {
            $gasMeter->client_name = $gasMeter->client ? $gasMeter->client->full_name : null;
        }

        return response()->json($gasMeters);
    }

    public function get(int $id): JsonResponse
    {
        $gasMeter = GasMeter::where('id', $id)->firstOrFail();

        $gasMeter->client_name = $gasMeter->client ? $gasMeter->client->full_name : null;

        return response()->json(['gasMeter' => $gasMeter]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'client_id' => 'exists:App\Models\Client,id',
        ]);
        $gasMeter = new GasMeter();
        $gasMeter->client_id = $request->input('client_id');
        $gasMeter->save();

        $gasMeter->client_name = $gasMeter->client ? $gasMeter->client->full_name : null;

        return response()->json(['gasMeter' => $gasMeter, 'message' => 'Created'], 200);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $this->validate($request, [
            'client_id' => 'exists:App\Models\Client,id',
        ]);
        /** @var GasMeter $gasMeter */
        $gasMeter = GasMeter::where('id', $id)->firstOrFail();
        $gasMeter->client_id = $request->input('client_id');
        $gasMeter->save();

        $gasMeter->client_name = $gasMeter->client ? $gasMeter->client->full_name : null;

        return response()->json(['gasMeter' => $gasMeter, 'message' => 'Updated'], 200);
    }

    public function delete(int $id): JsonResponse
    {
        $gasMeter = GasMeter::where('id', $id)->firstOrFail();
        $gasMeter->delete();

        return response()->json(['message' => 'Deleted'], 200);
    }
}
