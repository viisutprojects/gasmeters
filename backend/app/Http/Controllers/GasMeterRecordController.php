<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class GasMeterRecordController extends Controller
{

    /**
     * Get records for past hour and aggregate consumptions under individual minutes
     *
     * @return JsonResponse
     */
    public function hour(): JsonResponse
    {
        $sql = <<<SQL
            SELECT
                SUM(increase) as consumption,
                minute
            FROM (
                SELECT
                    id,
                    increase,
                    DATE_FORMAT(timestamp, "%k:%i") as minute
                FROM gas_meter_records WHERE timestamp > :timestamp
                ORDER BY timestamp DESC
            ) sub GROUP BY minute
        SQL;

        $timestamp = Carbon::now()->subHour();
        $records = DB::select($sql, ['timestamp' => $timestamp]);

        $result = [];
        foreach ($records as $record) {
            $result[$record->minute] = $record->consumption;
        }

        return response()->json($result);
    }

    /**
     * Get all meters and their latest record for clients. Aggregate values for each client incase they have multiple meters.
     *
     * @return JsonResponse
     */
    public function totalConsumptionByClients(): JsonResponse
    {
        $sql = <<<SQL
            SELECT client_id, full_name, SUM(consumption) as consumption FROM (
                SELECT c.id as client_id, c.full_name, gmr.record as consumption
                FROM clients c
                INNER JOIN gas_meters gm ON gm.client_id = c.id
                INNER JOIN gas_meter_records gmr ON gmr.gas_meter_id = gm.id AND gmr.id = (SELECT MAX(id) FROM gas_meter_records WHERE gas_meter_id = gm.id)
            ) client_meters GROUP BY client_id
        SQL;

        $records = DB::select($sql);

        $result = [];
        foreach ($records as $record) {
            $result[$record->full_name] = $record->consumption;
        }

        return response()->json($result);
    }
}
