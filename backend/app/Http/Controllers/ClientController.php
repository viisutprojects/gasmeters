<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ClientController extends Controller
{
    public function list(): JsonResponse
    {
        $clients = Client::all();

        return response()->json($clients);
    }

    public function get(int $id): JsonResponse
    {
        $client = Client::where('id', $id)->firstOrFail();

        return response()->json(['client' => $client]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'full_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ]);

        $client = new Client();
        $client->full_name = $request->input('full_name');
        $client->address = $request->input('address');
        $client->save();

        return response()->json(['client' => $client, 'message' => 'Created'], 200);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $this->validate($request, [
            'full_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ]);

        $client = Client::where('id', $id)->firstOrFail();
        $client->full_name = $request->input('full_name');
        $client->address = $request->input('address');
        $client->save();

        return response()->json(['client' => $client, 'message' => 'Updated'], 200);
    }

    public function delete(int $id): JsonResponse
    {
        $client = Client::where('id', $id)->firstOrFail();
        $client->delete();

        return response()->json(['message' => 'Deleted'], 200);
    }
}
