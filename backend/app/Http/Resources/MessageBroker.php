<?php

namespace App\Http\Resources;


use Bluerhinos\phpMQTT;

class MessageBroker
{
    private const SERVER = 'activemq';
    private const PORT = 1883;
    private const TOPIC = 'gasMeter/reading/update';

    private phpMQTT $mqtt;

    public function __construct()
    {
        $client_id = uniqid('', true);

        $this->mqtt = new phpMQTT(self::SERVER, self::PORT, $client_id);
    }

    public function getMqtt(): phpMQTT
    {
        return $this->mqtt;
    }

    public function connect(): bool
    {
        return $this->mqtt->connect();
    }

    public function subscribe(): void
    {
        $topics[self::TOPIC] = array('qos' => 0, 'function' => '__direct_return_message__');
        $this->mqtt->subscribe($topics, 0);
    }

    public function publish(array $data): void
    {
        $this->mqtt->publish(self::TOPIC, json_encode($data), 0, false);
    }

    public function disconnect(): void
    {
        $this->mqtt->close();
    }
}
