<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property float $record
 * @property float $increase
 * @property int $gas_meter_id
 * @property int $timestamp
 */
class GasMeterRecord extends Model
{
    use SoftDeletes;

    public function gasMeter(): BelongsTo
    {
        return $this->belongsTo(GasMeter::class);
    }
}
