<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int id
 * @property int client_id
 */
class GasMeter extends Model
{
    use SoftDeletes;

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    public function records(): HasMany
    {
        return $this->hasMany(GasMeterRecord::class);
    }

    /**
     * @return GasMeterRecord|NULL
     */
    public function latestRecord(): ?GasMeterRecord
    {
        return $this->records()->orderBy('timestamp', 'desc')->first();
    }
}
