import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './../store'

Vue.use(VueRouter)

const routes = [
    {
        path: '/login',
        name: 'LogIn',
        component: () => import(/* webpackChunkName: "login" */ '../views/Login'),
        meta: { hiddenForAuth: true },
    },
    {
        path: '/',
        name: 'Dashboard',
        component: () => import(/* webpackChunkName: "dashboard" */ '../views/Dashboard'),
        meta: { requiresAuth: true },
    },
    {
        path: '/settings',
        name: 'Settings',
        component: () => import(/* webpackChunkName: "settings" */ '../views/Settings'),
        meta: { requiresAuth: true },
    },
    {
        path: '/settings/clients',
        name: 'settings-clients',
        component: () => import(/* webpackChunkName: "clients" */ '../views/Settings/Clients'),
        meta: { requiresAuth: true },
    },
    {
        path: '/settings/gas-meters',
        name: 'settings-gas-meters',
        component: () => import(/* webpackChunkName: "gasMeters" */ '../views/Settings/GasMeters'),
        meta: { requiresAuth: true },
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters["auth/isLoggedIn"]) {
            next()
            return
        }
        next('/login')
    }
    else if (to.matched.some(record => record.meta.hiddenForAuth)) {
        if (!store.getters["auth/isLoggedIn"]) {
            next()
            return
        }
        next('/')
    }
    else {
        next()
    }
})

export default router
