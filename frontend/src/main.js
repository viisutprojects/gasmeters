import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import "./plugins/vuetify-mask.js";
import api from './services/api'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false
Vue.prototype.$http = api;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
