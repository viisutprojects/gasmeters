import axios from 'axios';
import store from './../store'

const instance = axios.create({
    baseURL: 'http://localhost:32900/api',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    // timeout: 10000,
    // params: {}
});

// Request interceptor
instance.interceptors.request.use(function (config) {
    config.headers.Authorization = 'bearer ' + localStorage.getItem('token')

    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error)
});

// Response interceptor
instance.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {

    if (error.response.status === 401 && error.response.config && !error.response.config.__isRetryRequest) {
        store.dispatch("auth/logout")
    }

    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
});



export default instance