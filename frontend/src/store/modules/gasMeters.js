//import axios from 'axios'
import api from '../../services/api'

const state = () => ({
    //gasMeters: JSON.parse(localStorage.getItem('gasMeters')) || [],
    gasMeters: [],
    availableClients: [],
    hourData: false,
    clientsConsumptionData: false,
    current: false,
})

// actions
const actions = {
    async save({commit}, payload) {
        return new Promise(((resolve, reject) => {
            let editedIndex = payload.editedIndex
            let editedItem = payload.editedItem

            // If index exits, then update existing record, otherwise insert new.
            if (editedIndex > -1) {
                api.put('/gas-meter/' + editedItem.id, editedItem)
                    .then(response => {
                        commit('save', {'editedIndex': editedIndex, 'editedItem': response.data.gasMeter})
                        resolve(response)
                    })
                    .catch(function (error) {
                        reject(error)
                    })
            } else {
                api.post('/gas-meter', editedItem)
                    .then(response => {
                        commit('save', {'editedIndex': editedIndex, 'editedItem': response.data.gasMeter})
                        resolve(response)
                    })
                    .catch(function (error) {
                        reject(error)
                    })
            }
        }))
    },

    async delete({commit}, payload) {
        return new Promise(((resolve, reject) => {
            let editedItem = payload.editedItem

            api.delete('/gas-meter/' + editedItem.id)
                .then(response => {
                    commit('delete', payload)
                    resolve(response)
                })
                .catch(function (error) {
                    reject(error)
                })
        }))
    },

    async fetchAll({commit}) {
        const response = await api.get('/gas-meter')

        commit('setItems', response.data)
    },

    async fetchAvailableClients({commit}) {
        const response = await api.get('/client')

        commit('setClients', response.data)
    },

    async fetchLastHourConsumptionData({commit}) {
        const response = await api.get('/data/hour')

        commit('setHourData', response.data)
    },

    async fetchClientsConsumptionData({commit}) {
        const response = await api.get('/data/client')

        commit('setClientsConsumptionData', response.data)
    },
}

// mutations
const mutations = {
    save: (state, payload) => {
        let editedIndex = payload.editedIndex
        let editedItem = payload.editedItem

        if (editedIndex > -1) {
            Object.assign(state.gasMeters[editedIndex], editedItem)
        } else {
            state.gasMeters.push(editedItem)
        }
    },

    delete: (state, payload) => {
        let editedIndex = payload.editedIndex
        state.gasMeters.splice(editedIndex, 1)
    },

    setItems: (state, gasMeters) => (state.gasMeters = gasMeters),
    setClients: (state, software) => (state.availableClients = software),
    setHourData: (state, data) => (state.hourData = data),
    setClientsConsumptionData: (state, data) => (state.clientsConsumptionData = data),
}

const getters = {
    gasMeters: state => state.gasMeters,
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}