import axios from 'axios'
import api from '../../services/api'

const state = () => ({
    authenticated: false,
    status: '',
    token: localStorage.getItem('token') || '',
    user: {},
})

// actions
const actions = {
    login({commit}, payload) {
        return new Promise(((resolve, reject) => {
            commit('auth_request')

            api.post('/login', payload)
                .then(response => {
                    const token = response.data.token
                    const user = response.data.user
                    localStorage.setItem('token', token)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('auth_success', token, user)
                    resolve(response)
                })
                .catch(function (error) {
                    commit('auth_error')
                    localStorage.removeItem('token')
                    reject(error)
                })
        }))
    },
    register({commit}, payload) {
        return new Promise((resolve, reject) => {
            commit('auth_request')
            api.post('/register', payload)
                .then(response => {
                    const token = response.data.token
                    const user = response.data.user
                    localStorage.setItem('token', token)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('auth_success', token, user)
                    resolve(response)
                })
                .catch(error => {
                    commit('auth_error', error)
                    localStorage.removeItem('token')
                    reject(error)
                })
        })
    },
    logout({commit}) {
        return new Promise((resolve) => {
            commit('logout')
            localStorage.removeItem('token')
            delete axios.defaults.headers.common['Authorization']
            //this.$router.push('/login')
            resolve()
        })
    }
}

// mutations
const mutations = {
    auth_request(state){
        state.status = 'loading'
    },
    auth_success(state, token, user){
        state.status = 'success'
        state.token = token
        state.user = user
    },
    auth_error(state){
        state.status = 'error'
    },
    logout(state){
        state.status = ''
        state.token = ''
    },
}

const getters = {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}