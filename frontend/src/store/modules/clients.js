//import axios from 'axios'
import api from '../../services/api'

const state = () => ({
    clients: [],
    current: false,
})

// actions
const actions = {
    async save({commit}, payload) {
        return new Promise(((resolve, reject) => {
            let editedIndex = payload.editedIndex
            let editedItem = payload.editedItem

            // If index exits, then update existing record, otherwise insert new.
            if (editedIndex > -1) {
                api.put('/client/' + editedItem.id, editedItem)
                    .then(response => {
                        commit('save', {'editedIndex': editedIndex, 'editedItem': response.data.client})
                        resolve(response)
                    })
                    .catch(function (error) {
                        reject(error)
                    })
            } else {
                api.post('/client', editedItem)
                    .then(response => {
                        commit('save', {'editedIndex': editedIndex, 'editedItem': response.data.client})
                        resolve(response)
                    })
                    .catch(function (error) {
                        reject(error)
                    })
            }
        }))
    },

    async delete({commit}, payload) {
        return new Promise(((resolve, reject) => {
            let editedItem = payload.editedItem

            api.delete('/client/' + editedItem.id)
                .then(response => {
                    commit('delete', payload)
                    resolve(response)
                })
                .catch(function (error) {
                    reject(error)
                })
        }))
    },

    async fetchAll({commit}) {
        const response = await api.get('/client')

        commit('setItems', response.data)
    },
}

// mutations
const mutations = {
    save: (state, payload) => {
        let editedIndex = payload.editedIndex
        let editedItem = payload.editedItem

        if (editedIndex > -1) {
            Object.assign(state.clients[editedIndex], editedItem)
        } else {
            state.clients.push(editedItem)
        }
    },

    delete: (state, payload) => {
        let editedIndex = payload.editedIndex
        state.clients.splice(editedIndex, 1)
    },

    setItems: (state, clients) => (state.clients = clients),
}

const getters = {
    clients: state => state.clients,
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}