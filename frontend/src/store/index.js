import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import clients from './modules/clients'
import gasMeters from './modules/gasMeters'
/*import createLogger from '../../../src/plugins/logger'*/

Vue.use(Vuex)

/*const debug = process.env.NODE_ENV !== 'production'*/

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    clients,
    gasMeters,
  },
  /*strict: debug,*/
  /*plugins: debug ? [createLogger()] : []*/
})
