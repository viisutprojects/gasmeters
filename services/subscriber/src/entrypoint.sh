#!/bin/bash

composer install &
chown www-data:www-data /usr/local/apache2/htdocs/vendor -R &
exec /usr/local/bin/php /usr/local/apache2/htdocs/artisan meter:subscribe
