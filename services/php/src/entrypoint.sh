#!/bin/bash

# Add local user.
USER_ID=${LOCAL_USER_ID:-9001}
useradd --shell /bin/bash -u $USER_ID -o -c "" -m container

composer install &
chown container:container /usr/local/apache2/htdocs/vendor -R &
php /usr/local/apache2/htdocs/artisan migrate &
crontab /etc/cron.d/gasmeter &
cron -f &
exec docker-php-entrypoint php-fpm
